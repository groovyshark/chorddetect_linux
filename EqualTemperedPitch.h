//
// Created by rzhevskiy on 29.07.16.
//

#ifndef CHORDDETECTOR_EQUALTEMPEREDPITCH_H
#define CHORDDETECTOR_EQUALTEMPEREDPITCH_H

#include "types.h"
#include "constants.h"
#include <math.h>
#include <iterator>

class EqualTemperedPitch
{
public:

    EqualTemperedPitch()
    {
        initilizeTransformationMatrix();
    }


    ChromaType calculateCentroidVector(const ChromaType& src);

    ChromaType harmonicChangeDetection(const ChromaVector& chromaVector);

private:
    void initilizeTransformationMatrix();

    double l1Norm(const ChromaType& src);

    const double r1 = 1.0;
    const double r2 = 1.0;
    const double r3 = 0.5;

    double transformationMatrix[transformMatrixH][transformMatrixW];
};


#endif //CHORDDETECTOR_EQUALTEMPEREDPITCH_H
