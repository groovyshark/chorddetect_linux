#include "WavFile.h"

void WavFile::load(const std::string& filename, StereoChannel channel)
{
    _filename = filename;
    _data.clear();
    ChannelType pacifier;
    if (LEFT == channel)
    {
        readHeaderAndData(_header, _data, pacifier);
    }
    else
    {
        readHeaderAndData(_header, pacifier, _data);
    }
    
    _sampleFrequency = _header.SampFreq;
}

void WavFile::readHeaderAndData(WavHeader& header,
        ChannelType& leftChannel, ChannelType& rightChannel)
{
    std::fstream fs;

    fs.open(_filename.c_str(), std::ios::in | std::ios::binary);

    if (!fs.is_open())
    {
        _filename.clear();
        return;
    }

    fs.read((char*)(&header), sizeof(WavHeader));

    short* data = new short[header.WaveSize / 2];
    
    fs.read((char*)data, header.WaveSize);
    fs.close();

    unsigned int channelSize = header.WaveSize / header.BytesPerSamp;
    leftChannel.resize(channelSize);
    
    if (header.Channels == 2)
        rightChannel.resize(channelSize);

    if (header.BitsPerSamp == 16)
    {
        if (header.Channels == 2)
            decode16bitStereo(leftChannel, rightChannel, data, channelSize);
        else
            decode16bit(leftChannel, data, channelSize);
    }
    else
    {
        if (header.Channels == 2)
            decode8bitStereo(leftChannel, rightChannel, data, channelSize);
        else
            decode8bit(leftChannel, data, channelSize);
    }

    delete[] data;
}



void WavFile::decode16bit(ChannelType& channel, short* data, std::size_t channelSize)
{
    for (int i = 0; i < channelSize; ++i)
        channel[i] = data[i];
}

void WavFile::decode16bitStereo(ChannelType& leftChannel, ChannelType& rightChannel, 
                                 short* data, std::size_t channelSize)
{
    for (int i = 0; i < channelSize; ++i)
    {
        leftChannel[i] = data[2 * i];
        rightChannel[i] = data[2 * i + 1];
    }
}


void WavFile::decode8bit(ChannelType& channel, short* data, std::size_t channelSize)
{
    unsigned char lb, hb;
    for (int i = 0; i < channelSize; ++i)
    {
        splitBytes(data[i / 2], lb, hb);
        channel[i] = lb - 128;
    }
}

void WavFile::decode8bitStereo(ChannelType& leftChannel, ChannelType& rightChannel, 
                                short* data, std::size_t channelSize)
{
    unsigned char lb, hb;
    for (std::size_t i = 0; i < channelSize; ++i)
    {
        splitBytes(data[i / 2], lb, hb);

        leftChannel[i] = lb - 128;
        rightChannel[i] = hb - 128;
    }
}


void WavFile::splitBytes(short twoBytes, unsigned char& lb, unsigned char& hb)
{
    lb = twoBytes & 0x00FF;
    hb = (twoBytes >> 8) & 0x00FF;
}

