#include "ScalarClassificator.h"

void ScalarClassificator::initializeTemplateMatrix()
{
    std::ifstream fin("chord_names.txt");

    if (!fin.is_open())
    {
        std::runtime_error("chord_names.txt not found");
        return;
    }

    int tone = 0;
    while (!fin.eof())
    {
        std::string templateString;
        std::getline(fin, templateString);

        auto start = templateString.find_first_not_of(" ");
        auto p = templateString.find_first_of(" ", start);
        
        int i = 0;
        while (p != std::string::npos)
        {
            Template mask(12);

            std::string name = templateString.substr(start, p - start);
            mask[i % 12] = mask[(i + (tone == 0 ? 4 : 3)) % 12] = mask[(i + 7) % 12] = (double)1 / (double)3;

            templateMatrix[name] = mask;

            start = templateString.find_first_not_of(" ", p);
            p = templateString.find_first_of(" ", start);

            ++i;
        }
        tone++;
    }
    fin.close();
}
    
std::string ScalarClassificator::detectChord(ChromaType& chromaVector)
{
    std::map < std::string, double > average;
    for (auto item : templateMatrix)
    {
        double sum = 0.0;
        for (int i = 0; i < item.second.size(); ++i)
            sum += item.second[i] * chromaVector[i];
        
        average[item.first] = sum;
    }

    auto max = std::max_element(average.begin(), average.end(), 
        [](std::pair<std::string, double> a, std::pair<std::string, double> b)
    {
        return a.second < b.second;
    });

    return max->first;
}