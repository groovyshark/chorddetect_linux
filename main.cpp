#include <gtkmm.h>
#include "MainWindow.h"

int main(int argc, char* argv[])
{
    // Initialize gtkmm
    Gtk::Main app(argc, argv);
    // Create the window
    MainWindow w;
    // Start main loop
    app.run(w);
    return 0;
}