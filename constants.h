//
// Created by rzhevskiy on 29.07.16.
//

#ifndef CHORDDETECTOR_CONSTANTS_H
#define CHORDDETECTOR_CONSTANTS_H

const int transformMatrixH = 6;
const int transformMatrixW = 12;


const int mainWindowWidth = 500;
const int mainWindowHeight = 400;

#endif //CHORDDETECTOR_CONSTANTS_H
