#ifndef SCALARCLASSIFICATOR_H
#define SCALARCLASSIFICATOR_H

#include "types.h"
#include <fstream>
#include <iostream>
#include <algorithm>

class ScalarClassificator
{
public:

    ScalarClassificator()
    {
        initializeTemplateMatrix();
    }
    
    std::string detectChord(ChromaType& chromaVector);

private:

    void initializeTemplateMatrix();

    TemplateType  templateMatrix;
};

#endif // SCALARCLASSIFICATOR_H
