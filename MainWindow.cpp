//
// Created by rzhevskiy on 29.07.16.
//

#include <iostream>
#include "MainWindow.h"
#include "constants.h"

MainWindow::MainWindow()
{
    this->resize(mainWindowWidth, mainWindowHeight);
    set_border_width(5);

    loadFileButton.add_label("Load file");
    loadFileButton.set_size_request(100,50);
    loadFileButton.signal_clicked().connect(sigc::mem_fun(*this,
                                                  &MainWindow::onButtonLoad));
    mainGrid.attach(loadFileButton, 0, 0, 1, 1);

    quitButton.add_label("Quit");
    quitButton.set_size_request(100,50);
    quitButton.signal_clicked().connect(sigc::mem_fun(*this,
                                                      &MainWindow::close));
    mainGrid.attach(quitButton, 0, 1, 1, 1);


    mainGrid.show_all();


    add(mainGrid);
}

void MainWindow::onButtonLoad()
{
    Gtk::FileChooserDialog dialog("Please choose a file",
                                  Gtk::FILE_CHOOSER_ACTION_OPEN);

    dialog.set_transient_for(*this);

    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("_Open", Gtk::RESPONSE_OK);

    Glib::RefPtr<Gtk::FileFilter> filter_wav = Gtk::FileFilter::create();
    filter_wav->set_name("WAV files");
    filter_wav->add_mime_type("audio/vnd.wave");
    dialog.add_filter(filter_wav);


    int result = dialog.run();
    switch(result)
    {
        case(Gtk::RESPONSE_OK):
        {
            std::cout << "Open clicked." << std::endl;
            //std::cout << "File selected: " <<  filename << std::endl;
            break;
        }
        case(Gtk::RESPONSE_CANCEL):
        {
            std::cout << "Cancel clicked." << std::endl;
            break;
        }
        default:
        {
            std::cout << "Unexpected button clicked." << std::endl;
            break;
        }
    }
}