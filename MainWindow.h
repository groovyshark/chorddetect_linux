//
// Created by rzhevskiy on 29.07.16.
//

#ifndef CHORDDETECTOR_MAINWINDOW_H
#define CHORDDETECTOR_MAINWINDOW_H

#include <gtkmm.h>


class MainWindow : public Gtk::Window
{
public:

    MainWindow();

    virtual ~MainWindow()
    {
    }

protected:

    void onButtonLoad();

    Gtk::Button loadFileButton;
    Gtk::Button quitButton;

    Gtk::Grid mainGrid;
};


#endif //CHORDDETECTOR_MAINWINDOW_H
