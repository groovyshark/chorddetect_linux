//
// Created by rzhevskiy on 29.07.16.
//

#include "EqualTemperedPitch.h"

void EqualTemperedPitch::initilizeTransformationMatrix()
{
    for (int d = 0; d < transformMatrixH; ++d)
        for (int l = 0; l < transformMatrixW; ++l)
            switch (d)
            {
                case 0:
                    transformationMatrix[d][l] = r1 * std::sin(l * ((7 * M_PI) / 6));
                    break;
                case 1:
                    transformationMatrix[d][l] = r1 * std::cos(l * ((7 * M_PI) / 6));
                    break;
                case 2:
                    transformationMatrix[d][l] = r2 * std::sin(l * ((3 * M_PI) / 2));
                    break;
                case 3:
                    transformationMatrix[d][l] = r2 * std::cos(l * ((3 * M_PI) / 2));
                    break;
                case 4:
                    transformationMatrix[d][l] = r3 * std::sin(l * ((2 * M_PI) / 3));
                    break;
                case 5:
                    transformationMatrix[d][l] = r3 * std::cos(l * ((2 * M_PI) / 3));
                    break;
                default:
                    break;
            }
}

ChromaType EqualTemperedPitch::calculateCentroidVector(const ChromaType& src)
{
    ChromaType centroidVector(transformMatrixH);
    for (int d = 0; d < transformMatrixH; ++d)
    {
        double sum = 0.0;
        for (int l = 0; l < transformMatrixW; ++l)
            sum += transformationMatrix[d][l] * src[l];

        centroidVector[d] = sum * (1 / l1Norm(src));
    }

    return centroidVector;
}

ChromaType EqualTemperedPitch::harmonicChangeDetection(const ChromaVector& chromaVector)
{
    ChromaType hcdf(chromaVector.size());

    double sum = 0.0;
    for (int i = 1; i < hcdf.size(); ++i)
    {
        for (int d = 0; d < transformMatrixH; ++d)
            sum += std::pow(chromaVector[i][d] - chromaVector[i - 1][d], 2);

        hcdf.push_back(std::sqrt(sum));
    }

    return hcdf;
}

double EqualTemperedPitch::l1Norm(const ChromaType& src)
{
    double sum = 0.0;
    for (auto item : src)
        sum += std::abs(item);

    return sum;
}