//
// Created by rzhevskiy on 29.07.16.
//

#ifndef CHORDDETECTOR_TYPES_H
#define CHORDDETECTOR_TYPES_H

#include <complex>
#include <valarray>
#include <map>
#include <vector>

typedef std::complex <double>	            Complex;
typedef std::valarray <Complex>	            Spectrum;


typedef std::vector <double>	            ChannelType;

typedef std::vector <double>	            ChromaType;

typedef std::vector <ChromaType>            ChromaVector;


typedef std::vector <double>                Template;
typedef std::map <std::string, Template>    TemplateType;

typedef std::vector <Spectrum>	            SpectrumArray;


#endif //CHORDDETECTOR_TYPES_H
