#ifndef WAVFILE_H
#define WAVFILE_H

#include <string>
#include <vector>
#include "types.h"
#include <fstream>

enum StereoChannel { LEFT, RIGHT };


struct WavHeader
{
    char   RIFF[4];
    std::uint32_t DataLength;
    char   WAVE[4];
    char   fmt_[4];
    std::uint32_t SubBlockLength;
    std::uint16_t formatTag;
    std::uint16_t Channels;
    std::uint32_t SampFreq;
    std::uint32_t BytesPerSec;
    std::uint16_t BytesPerSamp;
    std::uint16_t BitsPerSamp;
    char   data[4];
    std::uint32_t WaveSize;
};


class WavFile
{
public:
    WavFile()
    {
    }

    WavFile(const std::string& filename,
        StereoChannel channel = LEFT) :
        _filename(filename)
    {
        load(_filename, channel);
    }
    
    ~WavFile()
    {
    }

    void load(const std::string& filename, StereoChannel channel = LEFT);

    void readHeaderAndData(WavHeader& header, 
                ChannelType& leftChannel, ChannelType& rightChannel);



    static void decode16bit(ChannelType& channel,
        short* data, std::size_t channelSize);
    static void decode16bitStereo(ChannelType& leftChannel,
        ChannelType& rightChannel, short* data, std::size_t channelSize);

    static void decode8bit(ChannelType& channel,
        short* data, std::size_t channelSize);
    static void decode8bitStereo(ChannelType& leftChannel,
        ChannelType& rightChannel, short* data, std::size_t channelSize);


    std::size_t getSamplesCount() const
    {
        return _data.size();
    }

    unsigned int getBytesPerSec() const
    {
        return _header.BytesPerSec;
    }

    unsigned int getBytesPerSample() const
    {
        return _header.BytesPerSamp;
    }

    unsigned int getWaveSize() const
    {
        return _header.WaveSize;
    }

    unsigned short getBitsPerSample() const
    {
        return _header.BitsPerSamp;
    }

    std::string getFilename() const
    {
        return _filename;
    }

    double getSampleFrequency() const
    {
        return _sampleFrequency;
    }

    unsigned short getChannelsNum() const
    {
        return _header.Channels;
    }

    double sample(int pos) const
    {
        return _data[pos];
    }

    const double* toArray() const
    {
        return &_data[0];
    }

    std::size_t length() const
    {
        return getSamplesCount();
    }

private:
    static void splitBytes(short twoBytes, unsigned char& lb, unsigned char& hb);

    std::string _filename;

    WavHeader _header;
    ChannelType _data;

    double _sampleFrequency;
};

#endif // WAVFILE_H

